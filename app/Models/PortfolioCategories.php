<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioCategories extends Model
{
    protected $table = "portfolio_categories";
}
