<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Hash;
use Illuminate\Support\Facades\Redirect;

class RegisterController extends Controller
{
    public function viewRegister()
    {
        return view('website.auth.register');
    }

    public function registerUser(Request $request)
    {
        // dd($request);
        $request->validate([
            'full_name'             => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'password'              => 'min:8|required_with:confirm_password|same:confirm_password',
            'confirm_password'      => 'min:8',
        ]);

        $user               = new User;
        $user->name         = $request->full_name;
        $user->email        = $request->email; 
        $user->password     = Hash::make($request->password);
        $user->save();

        return Redirect::to('/login')->with('success','Your account is registered successfully!');

    }
}
