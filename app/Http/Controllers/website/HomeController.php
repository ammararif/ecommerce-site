<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Faqs;
use App\Models\About;
use App\Models\Portfolio;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('website.home');
    }
    public function about()
    {
        // dd("asdas");
        $about_data = About::all()->first();
        return view('website.about',compact('about_data'));
    }
    public function contact()
    {
        return view('website.contact');
    }

    public function shop()
    {
        return view('website.shop');
    }
    public function cart()
    {
        return view('website.cart');
    }
    public function checkout()
    {
        return view('website.checkout');
    }
    public function faqs()
    {
        $faqs = Faqs::all()->sortByDesc('id');
        return view('website.faqs',compact('faqs'));
    }
    public function blog()
    {
        return view('website.blog');
    }
    public function wishlist()
    {
        return view('website.wishlist');
    }
    public function error()
    {
        return view('website.404');
    }
    public function portfolio()
    {
        $portfolios = Portfolio::all()->sortByDesc('id');
        return view('website.portfolio',compact('portfolios'));
    }
}
