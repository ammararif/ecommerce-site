<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Portfolio;
use App\Models\PortfolioCategories;


class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolio_data = Portfolio::all()->sortByDesc('id');
        return view('admin.portfolio.view',compact('portfolio_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $portfolio_cats= PortfolioCategories::all();
        return view('admin.portfolio.create',compact('portfolio_cats'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'                => 'required',
            'portfolio_categories' => 'required'
        ]);

        if($request->hasFile('image')){
            $image=$request->file('image');
            $reImage=time().'.'.$image->getClientOriginalExtension();
            $dest=public_path('/bege-v4/bege/images/portfolio');
            $image->move($dest,$reImage);
        }
        else{
            $reImage='NA';
        }

        try{
            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $portfolio_data = Portfolio::find($request->id);
            }
            else{

                $success_msg = "Data is Successfully Added.";
                $portfolio_data = new Portfolio;
                
            }
            $portfolio_data->title                = $request->title;
            $portfolio_data->portfolio_cat_id     = $request->portfolio_categories;
            $portfolio_data->image                = $reImage;
            $portfolio_data->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Something is missing!');
        }

        // return \redirect()->back()->with('success',$success_msg);
        return redirect()->route('portfolio.view')->with('success',$success_msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio_cats= PortfolioCategories::all();
        $portfolio_data = Portfolio::find($id);
        return view('admin.portfolio.create',compact('portfolio_cats','portfolio_data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $portfolio_data = Portfolio::find($id);
            if($portfolio_data)
            {
                Portfolio::destroy($id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }
}
