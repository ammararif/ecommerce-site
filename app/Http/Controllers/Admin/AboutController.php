<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\About;

class AboutController extends Controller
{
    public function index(){
        $about_data = About::all()->sortByDesc('id');
        return view('admin.aboutus.view',compact('about_data'));
    }

    public function create()
    {
        return view('admin.aboutus.create');
    }

    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'title'                       => 'required',
            'description'                 => 'required',
            'happy_customer_counts'       => 'required',
            'awards_counts'               => 'required',
            'working_time'                => 'required',
            'reliable_products_counts'    => 'required'
        ]);

        if($request->hasFile('image')){
            $image=$request->file('image');
            $reImage=time().'.'.$image->getClientOriginalExtension();
            $dest=public_path('/bege-v4/bege/images/about');
            $image->move($dest,$reImage);
        }
        else{
            $reImage='NA';
        }

        try{
            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $about_data = About::find($request->id);
            }
            else{

                $success_msg = "Data is Successfully Added.";
                $about_data = new About;
                
            }
            $about_data->title                    = $request->title;
            $about_data->description              = $request->description;
            $about_data->image                    = $reImage;
            $about_data->happy_customer_counts    = $request->happy_customer_counts;
            $about_data->awards_counts            = $request->awards_counts;
            $about_data->working_time             = $request->working_time;
            $about_data->reliable_products_counts = $request->reliable_products_counts;
            $about_data->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Something is missing!');
        }

        // return \redirect()->back()->with('success',$success_msg);
        return redirect()->route('aboutus.view')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $about_data = About::find($id);
        return view('admin.aboutus.create',compact('about_data'));
    }

    public function delete($id)
    {
        try {
            $about_data = About::find($id);
            if($about_data)
            {
                About::destroy($id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }
}
