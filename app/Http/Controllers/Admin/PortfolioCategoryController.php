<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PortfolioCategories;

class PortfolioCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolio_cat_data = PortfolioCategories::all()->sortByDesc('id');
        return view('admin.portfolio_category.view',compact('portfolio_cat_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.portfolio_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'portfolio_categories'        => 'required'
        ]);

        try{
            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $portfolio_cat_data = PortfolioCategories::find($request->id);
            }
            else{

                $success_msg = "Data is Successfully Added.";
                $portfolio_cat_data = new PortfolioCategories;
                
            }
            $portfolio_cat_data->portfolio_categories     = $request->portfolio_categories;
            $portfolio_cat_data->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Something is missing!');
        }

        // return \redirect()->back()->with('success',$success_msg);
        return redirect()->route('portfolio_cat.view')->with('success',$success_msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $portfolio_cat_data = PortfolioCategories::find($id);
        return view('admin.portfolio_cat.create',compact('portfolio_cat_data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $portfolio_cat_data = PortfolioCategories::find($id);
            if($portfolio_cat_data)
            {
                Portfolio::destroy($id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }
}
