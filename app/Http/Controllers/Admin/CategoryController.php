<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categories;
use DB;

class CategoryController extends Controller
{
    public function index()
    {
        $Categories = Categories::all();
        return view('admin.categories.categories');
    
    }
    public function addCategory(Request $req)
    {
        return view('admin.categories.addnewcategory');
    
    }

    public function storeCategory(Request $req)
    {
        $Category = new Categories;
        $Category->name = $req->name;
        $Category->slug = $req->slug;
        $Category->save();

        return back()->with('category_added','Category added successfully');
    
    }
}
