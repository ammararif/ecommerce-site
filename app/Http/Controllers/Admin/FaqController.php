<?php

namespace App\Http\Controllers\Admin;
use App\Models\Faqs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class FaqController extends Controller
{
    public function index(){
        $faqs = Faqs::all()->sortByDesc('id');
        return view('admin.faqs.view',compact('faqs'));
    }

    public function create()
    {
        return view('admin.faqs.create');
    }

    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'question'  => 'required',
            'answer'    => 'required',
        ]);

        try{
            if(isset($request->id))
            {
                $success_msg = "Data is Successfully Updated.";
                $faqs = Faqs::find($request->id);
            }
            else{

                $success_msg = "Data is Successfully Added.";
                $faqs = new Faqs;
                
            }
            $faqs->question  = $request->question;
            $faqs->answer    = $request->answer;
            $faqs->save();
        }
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Some thing is missing!');
        }

        // return \redirect()->back()->with('success',$success_msg);
        return redirect()->route('faqs.view')->with('success',$success_msg);
    }

    public function edit($id)
    {
        $faqs = Faqs::find($id);
        return view('admin.faqs.create',compact('faqs'));
    }

    public function delete($id)
    {
        try {
            $faqs = Faqs::find($id);
            if($faqs)
            {
                Faqs::destroy($id);
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error','Data is not Delete!');
            
        }
        
        return \redirect()->back()->with('success','Data is Successfully Deleted!');
    }
}
