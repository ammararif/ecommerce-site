@extends('website.layouts.app')
@section('content')
<div class="wrapper home-one single-product-page">
    <div class="breadcrumbs-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="woocommerce-breadcrumb">
                        <a href="{{url('/')}}">Home</a>
                        <span class="separator">/</span> 404
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="error_page_start">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2>OOPS! PAGE NOT BE FOUND</h2>
                    <p>Sorry but the page you are looking for does not exist, have been removed, name changed or is temporarity unavailable.</p>
                    <div class="search__sidbar">
                        <div class="input_form">
                            <input type="text" class="input_text" value="Search..." name="s" id="search_input">
                            <button class="button" type="submit" id="blogsearchsubmit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    <div class="hom_btn">
                        <a href="{{url('/')}}">Back to home page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection