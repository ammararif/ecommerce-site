<script src="https://code.jquery.com/jquery-3.2.1.min.js" crossorigin="anonymous"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script>window.jQuery || document.write('<script src="js/jquery-3.2.1.min.js"><\/script>')</script>
<script>
// $(".categorie-title").click(function(){
//   $(".categori-menu-list").toggle();
// });
var swiper = new Swiper(".mySwiper", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    autoplay: true,
    pagination: {
        el: ".swiper-pagination",
        clickable: true,
    },
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});

</script>

<script src="{{asset('bege-v4/bege/js/popper.min.js')}}"></script>
<script src="{{asset('bege-v4/bege/js/bootstrap.min.js')}}"></script>
<script src="{{asset('bege-v4/bege/js/jquery-nivo-slider.pack.js')}}"></script>
<script src="{{asset('bege-v4/bege/js/plugins.js')}}"></script>
<script src="{{asset('bege-v4/bege/js/main.js')}}"></script>
@stack('custom-script')


