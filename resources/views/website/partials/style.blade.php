<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Bege || Home</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{asset('bege-v4/bege/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/bundle.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/all.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/brands.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/brands.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/fontawesome.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="{{asset('backend/plugins/fontawesome-free/css/fontawesome.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/regular.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/solid.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/solid.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/svg-with-js.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/svg-with-js.min.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/v4-shims.css')}}" rel="stylesheet">
<link href="{{asset('backend/plugins/fontawesome-free/css/v4-shims.min.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/ionicons.min.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/nice-select.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/meanmenu.min.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/easyzoom.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/animate.min.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/magnific-popup.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/jquery-ui.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/nice-select.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/nivo-slider.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/slick-theme.css')}}" rel="stylesheet">
<link href= "{{asset('bege-v4/bege/css/css-plugins/slick.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins/owl.carousel.min.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/css-plugins-call.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/main.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/responsive.css')}}" rel="stylesheet">
<link href="{{asset('bege-v4/bege/css/colors.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
</head>
<!-- <style id="fit-vids-style">
.fluid-width-video-wrapper{
    width:100%;position:relative;padding:0;
    }
.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {
    position:absolute;top:0;left:0;width:100%;height:100%;}
</style> -->


@stack('custom-css')