
@extends('website.layouts.app')

@section('content')
<div class="wrapper home-one single-product-page">
<!--BreadCrumbs !-->
<div class="breadcrumbs-container">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <nav class="woocommerce-breadcrumb">
                                <a href="{{url('/')}}">Home</a>
                                <span class="separator">/</span> About
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

<!-- About Area Page !-->

            <div class="about-page-area">
				<div class="about__us_page_area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="banner_h2__left_image">
                                    <img alt="" src="{{ asset('/bege-v4/bege/images/about').'/'.$about_data->image }}">
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-6  col-xs-12">
                                <div class="banner_h2_Right_text">
                                    <div class="wpb_wrapper">
                                        {{-- @dd($about_data) --}}
                                        <h3>{{@$about_data->title}}</h3>
                                        <p>{{@$about_data->description}}</p>
                                        <p class="text-center">
                                            <a href="#"> View Work </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="funfact-area bg--white" id="funfact-area">
                    <div class="funfacts">
                        <div class="row no-gutters">

                            <!--  Single Funfact -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="funfact">
                                    <div class="fun__fact_img">
                                        <img src="{{asset('bege-v4/bege/images/icon-img/about-us-icon1.png')}}" alt="">
                                    </div>
                                    <div class="fun_fact_info">
                                        <h1>
                                            <span class="counter">{{@$about_data->happy_customer_counts}}</span>
                                        </h1>
                                        <h5>HAPPY CUSTOMERS</h5>
                                    </div>
                                </div>
                            </div>
                            <!--//  Single Funfact -->

                            <!--  Single Funfact -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="funfact">
                                    <div class="fun__fact_img">
                                        <img src="{{asset('bege-v4/bege/images/icon-img/about-us-icon2.png')}}" alt="">
                                    </div>
                                    <div class="fun_fact_info">
                                        <h1>
                                         <span class="counter">{{@$about_data->awards_counts}}</span>
                                        </h1>
                                         <h5>AWARDS WINNED</h5>
                                    </div>
                                </div>
                            </div>
                            <!--//  Single Funfact -->

                            <!--  Single Funfact -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="funfact">
                                    <div class="fun__fact_img">
                                        <img src="{{asset('bege-v4/bege/images/icon-img/about-us-icon3.png')}}" alt="">
                                    </div>
                                    <div class="fun_fact_info">
                                        <h1>
                                            <span class="counter">{{@$about_data->working_time}}</span>
                                        </h1>
                                        <h5>HOURS WORKED</h5>
                                    </div>
                                </div>
                            </div>
                            <!--//  Single Funfact -->

                            <!--  Single Funfact -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="funfact">
                                    <div class="fun__fact_img">
                                        <img src="{{asset('bege-v4/bege/images/icon-img/about-us-icon4.png')}}" alt="">
                                    </div>
                                    <div class="fun_fact_info">
                                        <h1>
                                            <span class="counter">{{@$about_data->reliable_products_counts}}</span>
                                        </h1>
                                        <h5>COMPLETE PROJECTS</h5>
                                    </div>
                                </div>
                            </div>
                            <!--//  Single Funfact -->

                        </div>
                    </div>
                </div>

               
                {{-- <div class="abou_skrill__area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="skrill_here">
                                    <h3>WE HAVE SKILLS TO SHOW</h3>
                                    <div class="ht-single-about">
                                        <div class="skill-bar">
                                            <div class="skill-bar-item">
                                                <span>WEB DESIGN</span>
                                                <div class="progress">
                                                    <div class="progress-bar wow fadeInLeft width80" data-progress="80%" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                                        <span class="text-top">80%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="skill-bar-item">
                                                <span>PHP &amp; JAVASCRIPT</span>
                                                <div class="progress">
                                                    <div class="progress-bar wow fadeInLeft width90" data-progress="90%" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                                        <span class="text-top">90%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="skill-bar-item">
                                                <span>HTML5 &amp; CSS3</span>
                                                <div class="progress">
                                                    <div class="progress-bar wow fadeInLeft width70" data-progress="70%" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                                        <span class="text-top">70%</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="skill-bar-item">
                                                <span>wordpress</span>
                                                <div class="progress">
                                                    <div class="progress-bar wow fadeInLeft width95" data-progress="95%" data-wow-duration="1.5s" data-wow-delay="1.2s">
                                                        <span class="text-top">95%</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-6 col-xs-12">
                                <div class="banner_h2__left_image lft_to_right">
                                    <img alt="" src="{{asset('bege-v4/bege/images/banner/about-us-img2.jpg')}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
			</div>

</div>
@endsection