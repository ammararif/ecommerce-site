@include('website.partials.style')
<!-- Header -->
<div class="wrapper home-four">
<header class="header-area">
<div class="header-top-area">
    <div class="container">
          <div class="row">
                <div class="col-lg-6 col-sm-6">
                         <div class="top-bar-left">
                           <!-- welcome -->
                     <div class="welcome">
                          <p>England's Fastest Online Shopping Destination</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6">
                                <div class="topbar-nav">
                                    <div class="wpb_wrapper">
                                        <!-- my account -->
                                        <div class="menu-my-account-container">
                                            <a href="#">My Account <i class="fa fa-angle-down"></i></a>
                                            <ul>
                                                <li><a href="my-account.html">My Account</a></li>
                                                <li><a href="login.html">Login</a></li>
                                                <li><a href="register.html">Register</a></li>
                                                <li><a href="checkout.html">Checkout</a></li>
                                        
                                            </ul>
                                        </div>
                                        <div class="switcher">
                                            <!-- language-menu -->
                                            <div class="language">
                                                <a href="#">
                                                    <img src="{{asset('bege-v4/bege/images/icons/en.png')}}" alt="language-selector">English
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul>
                                                    <li>
                                                        <a href="#">
                                                            <img src="{{asset('bege-v4/bege/images/icons/fr.png')}}" alt="French">
                                                            <span>French</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- currency-menu -->
                                            <div class="currency">
                                                <a href="#">$ USD<i class="fa fa-angle-down"></i></a>
                                                <ul>
                                                    <li><a href="#">€ EUR</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-middle-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-3 col-md-12">
                                <!-- site-logo -->
                                <div class="site-logo">
                                    <a href="{{url('/')}}"><img src="{{asset('bege-v4/bege/images/logo/logo-black.png')}}" alt=""></a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12">
                                <!-- header-search -->
                                <div class="header-search clearfix">
                                    <div class="category-select pull-right">
                                        <select class="nice-select-menu" style="display: none;">
                                            <option data-display="All Categories">All Categories</option>
                                            <option value="1">Decor &amp; Furniture</option>
                                            <option value="2">Electronics</option>
                                            <option value="3">Fashion &amp; clothings</option>
                                            <option value="4" disabled="">Sport &amp; Outdoors</option>
                                            <option value="5">Toy, Kids &amp; Baby</option>
                                        </select>
                                        <div class="nice-select nice-select-menu" tabindex="0">
                                        <span class="current">All Categories</span>
                                        <ul class="list">
                                        <li data-value="All Categories" data-display="All Categories" class="option selected">All Categories</li>
                                        <li data-value="1" class="option">Decor &amp; Furniture</li>
                                        <li data-value="2" class="option">Electronics</li>
                                        <li data-value="3" class="option">Fashion &amp; clothings</li>
                                        <li data-value="4" class="option disabled">Sport &amp; Outdoors</li>
                                        <li data-value="5" class="option">Toy, Kids &amp; Baby</li>
                                        </ul>
                                        </div> 
                                    </div> 
                                    <div class="header-search-form">
                                        <form action="#">
                                            <input type="text" name="search" placeholder="Search product...">
                                            <input type="submit" name="submit" value="Search">
                                        </form>                                    
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-12">
                                <!-- shop-cart-menu -->
                                <div class="shop-cart-menu pull-right">
                                    <ul>
                                        <li><a href="#">
                                            <span class="cart-icon">
                                                <i class="fa fa-shopping-bag"></i><sup>3</sup>
                                            </span>
                                            <span class="cart-text">
                                                <span class="cart-text-title">My cart 2 testing <br> <strong>$ 145.00</strong> </span>
                                            </span>
                                        </a>
                                            <ul>
                                                <li>
                                                    <!-- single-shop-cart-wrapper -->
                                                    <div class="single-shop-cart-wrapper">
                                                        <div class="shop-cart-img">
                                                            <a href="#"><img src="{{asset('bege-v4/bege/images/product/mini/1.jpg')}}" alt="Image of Product"></a>
                                                        </div>
                                                        <div class="shop-cart-info">
                                                            <h5><a href="cart.html">sport t-shirt men</a></h5>
                                                            <span class="price">£515.00</span>
                                                            <span class="quantaty">Qty: 1</span>
                                                            <span class="cart-remove"><a href="#"><i class="fa fa-times"></i></a></span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <!-- single-shop-cart-wrapper -->
                                                    <div class="single-shop-cart-wrapper">
                                                        <div class="shop-cart-img">
                                                            <a href="#"><img src="{{asset('bege-v4/bege/images/product/mini/2.jpg')}}" alt="Image of Product"></a>
                                                        </div>
                                                        <div class="shop-cart-info">
                                                            <h5><a href="cart.html">sport coat amet</a></h5>
                                                            <span class="price">£100.00</span>
                                                            <span class="quantaty">Qty: 1</span>
                                                            <span class="cart-remove"><a href="#"><i class="fa fa-times"></i></a></span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <!-- single-shop-cart-wrapper -->
                                                    <div class="single-shop-cart-wrapper">
                                                        <div class="shop-cart-img">
                                                            <a href="#"><img src="{{asset('bege-v4/bege/images/product/mini/3.jpg')}}" alt="Image of Product"></a>
                                                        </div>
                                                        <div class="shop-cart-info">
                                                            <h5><a href="cart.html">Pellentesque men</a></h5>
                                                            <span class="price">£265.00</span>
                                                            <span class="quantaty">Qty: 1</span>
                                                            <span class="cart-remove"><a href="#"><i class="fa fa-times"></i></a></span>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <!-- shop-cart-total -->
                                                    <div class="shop-cart-total">
                                                        <p>Subtotal: <span class="pull-right">£880.00</span></p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="shop-cart-btn">
                                                        <a href="{{url('checkout')}}">Checkout</a>
                                                        <a href="{{url('cart')}}" class="pull-right">View Cart</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header-bottom-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 hidden-md hidden-sm pull-left category-wrapper">
                                <div class="categori-menu">
                                <span class="categorie-title">Categories</span>
                                <nav>
                                    <ul class="categori-menu-list menu-hidden" >
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/1.png')}}" alt="menu-icon"></span>Electronics<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <!-- categori Mega-Menu Start -->
                                            <ul class="ht-dropdown megamenu first-megamenu">
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Cameras</li>
                                                        <li><a href="shop.html">Cords and Cables</a></li>
                                                        <li><a href="shop.html">gps accessories</a></li>
                                                        <li><a href="shop.html">Microphones</a></li>
                                                        <li><a href="shop.html">Wireless Transmitters</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Digital Cameras</li>
                                                        <li><a href="shop.html">Camera one</a></li>
                                                        <li><a href="shop.html">Camera two</a></li>
                                                        <li><a href="shop.html">Camera three</a></li>
                                                        <li><a href="shop.html">Camera four</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Digital Cameras</li>
                                                        <li><a href="shop.html">Camera one</a></li>
                                                        <li><a href="shop.html">Camera two</a></li>
                                                        <li><a href="shop.html">Camera three</a></li>
                                                        <li><a href="shop.html">Camera four</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                            </ul>
                                            <!-- categori Mega-Menu End -->
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/2.png')}}" alt="menu-icon"></span>Fashion<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <!-- categori Mega-Menu Start -->
                                            <ul class="ht-dropdown megamenu megamenu-two">
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Men’s Fashion</li>
                                                        <li><a href="shop.html">Blazers</a></li>
                                                        <li><a href="shop.html">Boots</a></li>
                                                        <li><a href="shop.html">pants</a></li>
                                                        <li><a href="shop.html">Tops &amp; Tees</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Women’s Fashion</li>
                                                        <li><a href="shop.html">Bags</a></li>
                                                        <li><a href="shop.html">Bottoms</a></li>
                                                        <li><a href="shop.html">Shirts</a></li>
                                                        <li><a href="shop.html">Tailored</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                            </ul>
                                            <!-- categori Mega-Menu End -->
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/3.png')}}" alt="menu-icon"></span>Home &amp; Kitchen<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <!-- categori Mega-Menu Start -->
                                            <ul class="ht-dropdown megamenu megamenu-two">
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Large Appliances</li>
                                                        <li><a href="shop.html">Armchairs</a></li>
                                                        <li><a href="shop.html">Bunk Bed</a></li>
                                                        <li><a href="shop.html">Mattress</a></li>
                                                        <li><a href="shop.html">Sideboard</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Small Appliances</li>
                                                        <li><a href="shop.html">Bootees Bags</a></li>
                                                        <li><a href="shop.html">Jackets</a></li>
                                                        <li><a href="shop.html">Shelf</a></li>
                                                        <li><a href="shop.html">Shoes</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                            </ul>
                                            <!-- categori Mega-Menu End -->
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/4.png')}}"alt="menu-icon"></span>Phones &amp; Tablets<i class="fa fa-angle-right" aria-hidden="true"></i>
                                                                    </a>
                                            <!-- categori Mega-Menu Start -->
                                            <ul class="ht-dropdown megamenu megamenu-two">
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Tablet</li>
                                                        <li><a href="shop.html">tablet one</a></li>
                                                        <li><a href="shop.html">tablet two</a></li>
                                                        <li><a href="shop.html">tablet three</a></li>
                                                        <li><a href="shop.html">tablet four</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Smartphone</li>
                                                        <li><a href="shop.html">phone one</a></li>
                                                        <li><a href="shop.html">phone two</a></li>
                                                        <li><a href="shop.html">phone three</a></li>
                                                        <li><a href="shop.html">phone four</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                            </ul>
                                            <!-- categori Mega-Menu End -->
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/5.png')}}"alt="menu-icon"></span>TV &amp; Video<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                            <!-- categori Mega-Menu Start -->
                                            <ul class="ht-dropdown megamenu megamenu-two">
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Gaming Desktops</li>
                                                        <li><a href="shop.html">Alpha Desktop</a></li>
                                                        <li><a href="shop.html">rober Desktop</a></li>
                                                        <li><a href="shop.html">Ultra Desktop </a></li>
                                                        <li><a href="shop.html">beta desktop</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                                <!-- Single Column Start -->
                                                <li class="single-megamenu">
                                                    <ul>
                                                        <li class="menu-tile">Women’s Fashion</li>
                                                        <li><a href="shop.html">3D-Capable</a></li>
                                                        <li><a href="shop.html">Clearance</a></li>
                                                        <li><a href="shop.html">Free Shipping Eligible</a></li>
                                                        <li><a href="shop.html">On Sale</a></li>
                                                    </ul>
                                                </li>
                                                <!-- Single Column End -->
                                            </ul>
                                            <!-- categori Mega-Menu End -->
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/6.png')}}" alt="menu-icon"></span>Beauty</a>
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/7.png')}}" alt="menu-icon"></span>Sport &amp; tourism</a>
                                        </li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/8.png')}}" alt="menu-icon"></span>Fruits &amp; Veggies</a></li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/9.png')}}" alt="menu-icon"></span>Computer &amp; Laptop</a></li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/10.png')}}"alt="menu-icon"></span>Meat &amp; Seafood</a></li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/12.png')}}" alt="menu-icon"></span>Samsung</a></li>
                                        <li><a href="shop.html"><span><img src="{{asset('bege-v4/bege/images/icons/11.png')}}" alt="menu-icon"></span>Sanyo</a></li>
                                    </ul>
                                </nav>
                            </div>
                            </div>
                            <div class="col-lg-9">
                                <!-- main-menu -->
                                <div class="main-menu">
                                    <nav>
                                        <ul>
                                            <li class="current"><a href="{{url('/')}}">Home</a>
                                                <!-- <ul class="submenu">
                                                    <li><a href="index.html">Home Shop 1</a></li>
                                                    <li><a href="index-2.html">Home Shop 2</a></li>
                                                    <li><a href="index-3.html">Home Shop 3</a></li>
                                                    <li><a href="index-4.html">Home Shop 4</a></li>
                                                </ul> -->
                                            </li>
                                            <li><a href="{{url('shop')}}">Shop</a></li>
                                            <li><a href="{{url('blog')}}">Blog</a></li>
                                            <li><a href="{{url('about')}}">About Us</a></li>
                                            <li><a href="{{url('contact')}}">Contact</a></li>
                                            <li><a href="#">Features <i class="fa fa-angle-down"></i></a>
                                                <ul class="megamenu-3-column">
                                                    <li><a href="#">Pages</a>
                                                        <ul>
                                                            <li><a href="{{url('about')}}">About Us</a></li>
                                                            <li><a href="{{url('contact')}}">Contact Us</a></li>
                                                            <li><a href="service.html">Services</a></li>
                                                            <li><a href="{{url('portfolio')}}">Portfolio</a></li>
                                                            <li><a href="faq.html">Frequently Questins</a></li>
                                                            <li><a href="404.html">Error 404</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Blog</a>
                                                        <ul>
                                                            <li><a href="blog-no-sidebar.html">None Sidebar</a></li>
                                                            <li><a href="blog.html">Sidebar right</a></li>
                                                            <li><a href="single-blog.html">Image Format</a></li>
                                                            <li><a href="single-blog-gallery.html">Gallery Format</a></li>
                                                            <li><a href="single-blog-audio.html">Audio Format</a></li>
                                                            <li><a href="single-blog-video.html">Video Format</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Shop</a>
                                                        <ul>
                                                            <li><a href="shop.html">Shop</a></li>
                                                            <li><a href="shop-list.html">Shop List View</a></li>
                                                            <li><a href="shop-right.html">Shop Right</a></li>
                                                            <li><a href="single-product.html">Shop Single</a></li>
                                                            <li><a href="cart.html">Shoping Cart</a></li>
                                                            <li><a href="checkout.html">Checkout</a></li>
                                                            <li><a href="my-account.html">My Account</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                                <div class="mobile-menu-area">
                                    <div class="mobile-menu">
                                        <nav id="mobile-menu-active" style="display: block;">
                                            <ul class="menu-overflow">
                                                <li><a href="{{url('/')}}">HOME</a>
                                                    <!-- <ul>
                                                        <li><a href="index.html">Home Shop 1</a></li>
                                                        <li><a href="index-2.html">Home Shop 2</a></li>
                                                        <li><a href="index-3.html">Home Shop 3</a></li>
                                                        <li><a href="{index-4.html}">Home</a></li>
                                                    </ul> -->
                                                </li>
                                                <li><a href="{{url('shop')}}">Shop</a></li>
                                                <li><a href="{{url('blog')}}">Blog</a></li>
                                                <li><a href="{{url('about')}}">About Us</a></li>
                                                <li><a href="{{url('contact')}}">Contact</a></li>
                                                <li><a href="#">Features</a>
                                                    <ul>
                                                        <li><a href="#">Pages</a>
                                                            <ul>
                                                                <li><a href="{{url('about')}}">About Us</a></li>
                                                                <!-- <li><a href="{{url('service')}}">Services</a></li> -->
                                                                <!-- <li><a href="service-2.html">Services Two</a></li> -->
                                                                <li><a href="{{url('portfolio')}}">Portfolio</a></li>
                                                                <li><a href="{{url('faqs')}}">Frequently Questins</a></li>
                                                                <li><a href="{{url('404')}}">Error 404</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Blog</a>
                                                            <ul>
                                                                <li><a href="blog-no-sidebar.html">None Sidebar</a></li>
                                                                <li><a href="blog.html">Sidebar right</a></li>
                                                                <li><a href="single-blog.html">Image Format</a></li>
                                                                <li><a href="single-blog-gallery.html">Gallery Format</a></li>
                                                                <li><a href="single-blog-audio.html">Audio Format</a></li>
                                                                <li><a href="single-blog-video.html">Video Format</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#">Shop</a>
                                                            <ul>
                                                                <li><a href="shop.html">Shop</a></li>
                                                                <li><a href="shop-list.html">Shop List View</a></li>
                                                                <li><a href="shop-right.html">Shop Right</a></li>
                                                                <li><a href="single-product.html">Shop Single</a></li>
                                                                <li><a href="cart.html">Shoping Cart</a></li>
                                                                <li><a href="checkout.html">Checkout</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </nav>                          
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
</header>


@yield('content')


<!-- Footer -->

<footer class="footer-area">
                <!-- newsletter area -->
                <div class="newsletter-area">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12 col-xl-8">
                                <div class="newsletter-text">
                                    <div class="icon"><i class="icon fa fa-envelope-o"></i></div>
                                    <h3>Sign Up To Newsletter</h3>
                                    <h4>..and Receive $29 Coupon For First Shopping</h4>
                                </div>
                                <div class="newsletter">
                                    <div class="newsletter-box">
                                         <form action="#">
                                              <input class="subscribe" placeholder="your email address" name="email" id="subscribe" type="email">
                                              <button type="submit" class="submit">subscribe!</button>
                                         </form>
                                     </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xl-4">
                                <div class="footer-social">
                                    <ul class="social-icons">
                                        <li><a class="facebook social-icon" href="#" title="Facebook" target="_blank"><i class="far fa-facebook"></i></a></li>
                                        <li><a class="twitter social-icon" href="#" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        <li><a class="instagram social-icon" href="#" title="Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        <li><a class="linkedin social-icon" href="#" title="Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a class="rss social-icon" href="#" title="Rss" target="_blank"><i class="fa fa-rss"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- newsletter area end -->
                <!-- footer main -->
                <div class="footer-main">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-12">
                                <div class="footer-logo-area">
                                    <ul>
                                        <li>
                                            <h4>Contact info:</h4>
                                            <p>169-C, Technohub, Dubai Silicon Oasis.</p>
                                        </li>
                                        <li>
                                            <h4>Telephone:</h4>
                                            <p>(+011) 123 777 8888 - (+011) 123 888 9999</p>
                                        </li>
                                        <li>
                                            <h4>Email:</h4>
                                            <p>Support@example.com</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-12">
                                <div class="footer-menu">
                                    <h3>Customer Service</h3>
                                    <ul>
                                        <li><a href="{{url('contact')}}"><i class="fa fa-angle-right pr-2"></i>Contact Us</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Returns</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Order History</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Site Map</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Testimonials</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>My Account</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Unsubscribe Notification</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-12">
                                <div class="footer-menu">
                                    <h3>Information</h3>
                                    <ul>
                                        <li><a href="{{url('about')}}"><i class="fa fa-angle-right pr-2"></i>About Us</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Delivery infomation</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Privacy Policy</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Terms &amp; Conditions</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Warranty</a></li>
                                        <li><a href="{{url('faqs')}}"><i class="fa fa-angle-right pr-2"></i>FAQ</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Seller Login</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-12">
                                <div class="footer-menu">
                                    <h3>Extras</h3>
                                    <ul>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Brands</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Gift Vouchers</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Affiliates</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Wishlist</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Order History</a></li>
                                        <li><a href="#"><i class="fa fa-angle-right pr-2"></i>Track Your Order</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer main end -->
                <!-- footer copyright area -->
                <div class="footer-copyright-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6 col-md-6">
                                <p>Copyright © 2018 Bege . All Rights Reserved.</p>
                            </div>
                            <div class="col-sm-12 col-lg-6 col-md-6 pull-right">
                                <img src="{{asset('bege-v4/bege/images/icons/payment-icon.png')}}" alt="payment icon">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer copyright area end -->
            </footer>
</div>

@include('website.partials.script')


