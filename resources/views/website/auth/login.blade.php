@extends('website.layouts.app')

@section('content')
<div class="wrapper home-one single-product-page">
    <div class="breadcrumbs-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="woocommerce-breadcrumb">
                        <a href="index.html">Home</a>
                        <span class="separator">/</span> Sign in
                    </nav>
                </div>
            </div>
        </div>
    </div>
    
    <div class="entry-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="entry-title">Sign In</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="login-page-area">
        <div class="signin-container">
            <div class="login-area">
                <!-- New Customer Start -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="well mb-sm-30">
                            <div class="new-customer">
                                <h3 class="custom-title">new customer</h3>
                                <p class="mtb-10"><strong>Register</strong></p>
                                <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made</p>
                                <a class="customer-btn" href="register.html">continue</a>
                            </div>
                        </div>
                    </div>
                    <!-- New Customer End -->
                    <!-- Returning Customer Start -->
                    <div class="col-md-6">
                        <div class="well">
                            <div class="return-customer">
                                <h3 class="mb-10 custom-title">returnng customer</h3>
                                <p class="mb-10"><strong>I am a returning customer</strong></p>
                                <form action="{{route('login.store')}}" method="post">
                                    @if (Session::has('message'))
                                    <p class="text-danger">{{Session::get('message')}}</p>
                                    @endif

                                    @if (Session::has('success'))
                                    <p class="text-success">{{Session::get('success')}}</p>
                                    @endif

                                    @csrf
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" placeholder="Enter your email address..." id="input-email" class="form-control" value="{{old('email')}}" name="email">
                                    </div>
                                    @error('email')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" name="password" placeholder="Password" id="input-password" class="form-control">
                                    </div>


                                    <div class="form-group icheck-primary">
                                        <label style="padding-left:0px!important;">Remember Me</label>
                                        <input type="checkbox" id="remember" name="remember">
                                    </div>


                                    <!-- <div class="icheck-primary">
                                    <input type="checkbox" id="remember" name="remember">
                                    <label for="remember">
                                        Remember Me
                                    </label>
                                    </div> -->

                                    @error('password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                    <p class="lost-password"><a href="forgot-password.html">Forgot password?</a></p>
                                    <input type="submit" value="Login" class="return-customer-btn">
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Returning Customer End -->
                </div>
            </div>
        </div>
    </div>
</div>         
@endsection
@push('custom-css')
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">

    <style>
    .signin-container{
        max-width: 880px;
        margin-right: auto;
        margin-left: auto;
    }
    .wrapper, body, html {
        min-height: 100%;
    }
    </style>
@endpush