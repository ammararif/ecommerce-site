@extends('website.layouts.app')
@section('content')
<div class="wrapper home-one single-product-page">
    <div class="breadcrumbs-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="woocommerce-breadcrumb">
                        <a href="index.html">Home</a>
                        <span class="separator">/</span> Register
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="entry-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="entry-title">Register</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="register-page-area">
        <div class="signup-container">
            <div class="row">
                <div class="col-sm-12">
                <form class="form-register" action="{{route('registerUser')}}" method="post">
                    @if (Session::has('message'))
                    <p class="text-danger">{{Session::get('message')}}</p>
                    @endif

                        @csrf
                        <fieldset>
                            <legend>Your Personal Details</legend>
                            <div class="form-group d-md-flex align-items-md-center">
                                <label class="control-label col-md-3" for="f-name"><span class="require">*</span>Full Name</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" value="{{old('full_name')}}" name="full_name" placeholder="Full Name">
                                </div>
                            </div>
                            @error('full_name')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                            <div class="form-group d-md-flex align-items-md-center">
                                <label class="control-label col-md-3" for="l-name"><span class="require">*</span>Email</label>
                                <div class="col-md-8">
                                    <input type="email" class="form-control" value="{{old('email')}}" name="email" placeholder="Email">
                                </div>
                            </div>
                            @error('email')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                            <div class="form-group d-md-flex align-items-md-center">
                                <label class="control-label col-md-3" for="email"><span class="require">*</span>Password</label>
                                <div class="col-md-8">
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                            </div>
                            @error('password')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                            <div class="form-group d-md-flex align-items-md-center">
                                <label class="control-label col-md-3" for="number"><span class="require">*</span>Confirm Password</label>
                                <div class="col-md-8">
                                <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password">
                                </div>
                            </div>
                            @error('confirm_password')
                            <p class="text-danger text-sm">{{$message}}</p>
                            @enderror
                        </fieldset>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button type="submit" class="btn btn-primary w-25 mt-3 mb-3 signup-btn">Sign Up</button>
                            </div>
                        </div>
                        <!-- /.login-box -->
                        <div class="col-md-12 text-center">
                            {{-- <a href="{{route('forgotpassword','user')}}">Forgot Password?</a>
                            <hr> --}}
                            <a href="{{route("login")}}">Sign In!</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('custom-css')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">

    <style>
        .wrapper, body, html {
            min-height: 100%;
        }
        .signup-container{
            max-width: 880px;
            margin-right: auto;
            margin-left: auto;
            background-color:#fff;
        }
        .register-page-area{
            background-color:#f5f5f5;
        }
        .form-register legend{
            padding:12px;
        }
        .signup-btn {
            background-color: #333333 !important;
            color: #fff;
            border: 1px solid #333333 !important
        }
        .signup-btn:hover {
            background-color: #12a4dd !important;
            color: #fff;
            border: 1px solid #12a4dd !important
        }
    </style>
@endpush

@push('custom-script')

    <!-- jQuery -->
    <script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>

@endpush