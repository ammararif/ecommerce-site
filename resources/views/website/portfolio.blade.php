@extends('website.layouts.app')
@section('content')
<div class="wrapper home-one single-product-page">
    <div class="breadcrumbs-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="woocommerce-breadcrumb">
                        <a href="{{url('/')}}">Home</a>
                        <span class="separator">/</span> Portfolio
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="entry-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="entry-title">Portfolio</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="co-portfolio-section-1">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-lg-12 col-md-12  text-center mb-75">
                    <div class="co-isotop-filter-1 isotop-filter">
                        @if(count($portfolios)>0)
                            <button id="all" class="portfolio_btn" data-filter="*">All</button>
                            @foreach($portfolios as $portfolio)
                                <button class=" " data-filter="*">{{$portfolio->portfolio_categories}}</button>
                            @endforeach
                        @else
                            <p class="alert-danger">No Categories Found</p>
                        @endif
                    </div>
                </div>
            </div>

            <div id="parent" class="co-isotop-grid-1 isotop-grid row" style="position: relative;height: 843.843px;">
                @if(count($portfolios)>0)
                    @foreach($portfolios as $portfolio)
                        <div class="co-isotop-item-1 isotop-item port_cat web col-lg-3 col-md-6 col-sm-6 col-xs-12" style="position: absolute;left: 0px;top: 0px;">
                            <div class="portfolio___single">
                                    <img src="{{asset('bege-v4/bege/images/portfolio').'/'.$portfolio->image}}" alt="">
                                    <div class="content">
                                        <div class="portfolio__icon">
                                            {{-- <a href="portfolio-details.html">
                                                <i class="fa fa-link pt-3"></i>
                                            </a> --}}
                                            <a class="image-popup" href="{{asset('bege-v4/bege/images/portfolio').'/'.$portfolio->image}}">
                                                <i class="fa fa-search pt-3"></i>
                                            </a>
                                        </div>
                                        <div class="title">{{$portfolio->title}}</div>
                                    </div>
                            </div>
                        </div>
                    @endforeach
                

                {{-- <div class="co-isotop-item-1 isotop-item mobile-app photography col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute;left: 450px;top: 0px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p3.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p3.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">There are many variations</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item illustration web col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 900px; top: 0px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p6.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p6.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">Coffee &amp; Cookie Time</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item photography branding col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 1350px; top: 0px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p8.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p8.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">passage of Lorem Ipsum</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item illustration mobile-app col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 0px; top: 281px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p8.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p8.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">looked up one of the more</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item branding web col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 450px; top: 281px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p2.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p2.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">If you are going to use</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item branding web col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 900px; top: 281px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p3.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p3.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">accompanied by English</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item mobile-app photography col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 1350px; top: 281px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p4.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p4.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">Coffee &amp; Cookie Time</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item illustration web col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 0px; top: 562px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p5.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p5.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">Coffee &amp; Cookie Time</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item photography branding col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 450px; top: 562px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p6.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p6.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">Coffee &amp; Cookie Time</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item illustration mobile-app col-lg-3 col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 900px; top: 562px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p7.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p7.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">Coffee &amp; Cookie Time</div>
                        </div>
                    </div>
                </div>

                <div class="co-isotop-item-1 isotop-item branding web col-lg-3  col-md-6 col-sm-6 col-xs-12 " style="position: absolute; left: 1350px; top: 562px;">
                    <div class="portfolio___single">
                        <img src="{{asset('bege-v4/bege/images/portfolio')}}/p8.jpg" alt="">
                        <div class="content">
                            <div class="portfolio__icon">
                                <a href="portfolio-details.html">
                                    <i class="fa fa-link pt-3"></i>
                                </a>
                                <a class=" image-popup" href="{{asset('bege-v4/bege/images/portfolio')}}/p8.jpg">
                                    <i class="fa fa-search pt-3"></i>
                                </a>
                            </div>
                            <div class="title">Coffee &amp; Cookie Time</div>
                        </div>
                    </div>
                </div> --}}
            </div>
            @else
                <div class="col-md-12 col-sm-12">    
                    <div class="row">
                        <div class="col-md-12 col-sm-12">    
                            <p class="alert-danger text-center">No Portfolio Found</p>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>

@endsection
@push('custom-script')
<script>
    var $btns = $('.portfolio_btn').click(function() {
    if (this.id == 'all') {
        $('#parent > div.port_cat').fadeIn(450);
    } 
    else {
        var $el = $('.portfolio_btn' + this.id).fadeIn(450);
        $('#parent > div.port_cat').not($el).hide();
    }
    $btns.removeClass('active');
    $(this).addClass('active');
    }) 
</script>
@endpush