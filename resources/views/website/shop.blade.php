@extends('website.layouts.app')
@section('content')
<div class="wrapper home-one single-product-page">
    <div class="breadcrumbs-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="woocommerce-breadcrumb">
                        <a href="{{url('/')}}">Home</a>
                        <span class="separator">/</span> Shop
                    </nav>
                </div>
            </div>
        </div>
    </div>


    <div class="shop-page-wraper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-3 sidebar-shop">
                    <div class="sidebar-product-categori">
                        <div class="widget-title">
                            <h3>PRODUCT CATEGORIES</h3> </div>
                        <div class="widget-content">
                            <ul class="product-categories">
                                <li class="cat-item"> <a href="#">Accessories</a> <span class="count">(5)</span> </li>
                                <li class="cat-item"> <a href="#">Bedroom</a> <span class="count">(4)</span> </li>
                                <li class="cat-item"> <a href="#">Decor &amp; Furniture</a> <span class="count">(12)</span> </li>
                                <li class="cat-item"> <a href="#">Electronics &amp; Computer</a> <span class="count">(13)</span> </li>
                                <li class="cat-item"> <a href="#">Fashion &amp; clothings</a> <span class="count">(13)</span> </li>
                                <li class="cat-item"> <a href="#">Furniture</a> <span class="count">(4)</span> </li>
                                <li class="cat-item"> <a href="#">Home, Garden &amp; Tools</a> <span class="count">(14)</span> </li>
                                <li class="cat-item"> <a href="#">Laptops &amp; Desktops</a> <span class="count">(7)</span> </li>
                                <li class="cat-item"> <a href="#">Livingroom</a> <span class="count">(4)</span> </li>
                                <li class="cat-item"> <a href="#">Men</a> <span class="count">(4)</span> </li>
                                <li class="cat-item"> <a href="#">Mobiles &amp; Tablets</a> <span class="count">(6)</span> </li>
                                <li class="cat-item"> <a href="#">Sport &amp; Outdoors</a> <span class="count">(7)</span> </li>
                                <li class="cat-item"> <a href="#">Toy, Kids &amp; Baby</a> <span class="count">(7)</span> </li>
                                <li class="cat-item"> <a href="#">Uncategorized</a> <span class="count">(0)</span> </li>
                                <li class="cat-item"> <a href="#">Women</a> <span class="count">(4)</span> </li>
                            </ul>
                        </div>
                        <div class="product-filter mb-30">
                            <div class="widget-title">
                                <h3>Filter by price</h3> </div>
                            <div class="widget-content">
                                <div id="price-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span></div>
                                <div class="price-values">
                                    <div class="price_text_btn"> <span>Price:</span>
                                        <input type="text" class="price-amount"> </div>
                                    <button class="button" type="submit">Filter</button>
                                </div>
                            </div>
                        </div>
                        <div class="widget-title">
                            <h3>SELECT BY COLOR</h3> </div>
                        <div class="widget-content">
                            <ul class="product-categories">
                                <li class="cat-item"> <a href="#">Gold</a> <span class="count">(1)</span> </li>
                                <li class="cat-item"> <a href="#">Green</a> <span class="count">(1)</span> </li>
                                <li class="cat-item"> <a href="#">White</a> <span class="count">(1)</span> </li>
                            </ul>
                        </div>
                        <div class="product-filter mb-30">
                            <div class="widget-title">
                                <h3>TOP RATED PRODUCTS</h3> </div>
                            <div class="widget-content">
                                <ul class="product_list_widget">
                                    <li class="widget-mini-product">
                                        <div class="product-image">
                                            <a title="Phasellus vel hendrerit" href="#"> <img alt="" src="{{asset('bege-v4/bege/images/product')}}/2.jpg"> </a>
                                        </div>
                                        <div class="product-info">
                                            <a title="Phasellus vel hendrerit" href="#"> <span class="product-title">Consequuntur magni</span> </a>
                                            <div class="star-rating">
                                                <div class="rating-box"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> </div>
                                            </div> <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">$</span>55.00</span>
                                        </div>
                                    </li>
                                    <li class="widget-mini-product">
                                        <div class="product-image">
                                            <a title="Phasellus vel hendrerit" href="#"> <img alt="" src="{{asset('bege-v4/bege/images/product')}}/3.jpg"> </a>
                                        </div>
                                        <div class="product-info">
                                            <a title="Phasellus vel hendrerit" href="#"> <span class="product-title">Aliquam lobortis</span> </a>
                                            <div class="star-rating">
                                                <div class="rating-box"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> </div>
                                            </div> <span class="woocommerce-Price-amount amount">
                                                            <span class="woocommerce-Price-currencySymbol">$</span>55.00</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="sidebar-single-banner">
                            <a href="#"> <img src="{{asset('bege-v4/bege/images/banner')}}/shop-sidebar.jpg" alt="Banner"> </a>
                        </div>
                        <div class="sidebar-tag">
                            <div class="widget-title">
                                <h3>PRODUCT TAGS</h3> </div>
                            <div class="widget-content">
                                <div class="product-tags"> <a href="#">New </a> <a href="#">brand</a> <a href="#">black</a> <a href="#">white</a> <a href="#">chire</a> <a href="#">table</a> <a href="#">Lorem</a> <a href="#">ipsum</a> <a href="#">dolor</a> <a href="#">sit</a> <a href="#">amet</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-9 shop-content">
                    <div class="shop-banner"> <img src="{{asset('bege-v4/bege/images/banner')}}/shop-category.jpg" alt=""> </div>
                    <div class="product-toolbar">
                        <div class="topbar-title">
                            <h1>Shop</h1> </div>
                        <div class="product-toolbar-inner">
                            <div class="product-view-mode">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#grid"><i class="fa fa-th" aria-hidden="true"></i></a></li>
                                    <li><a data-toggle="tab" href="#list"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <p class="woocommerce-result-count">Showing 1–12 of 42 results</p>
                            <div class="woocommerce-ordering">
                                <form method="get" class="woocommerce-ordering hidden-xs">
                                    <div class="orderby-wrapper">
                                        <label>Sort By :</label>
                                        <select class="nice-select-menu orderby" style="display: none;">
                                            <option dara-display="Select">Default sorting</option>
                                            <option value="popularity">Sort by popularity</option>
                                            <option value="rating">Sort by average rating</option>
                                            <option value="date">Sort by newness</option>
                                            <option value="price">Sort by price: low to high</option>
                                            <option value="price-desc">Sort by price: high to low</option>
                                        </select>
                                        <div class="nice-select nice-select-menu orderby" tabindex="0"><span class="current">Default sorting</span>
                                            <ul class="list">
                                                <li data-value="Default sorting" class="option selected">Default sorting</li>
                                                <li data-value="popularity" class="option">Sort by popularity</li>
                                                <li data-value="rating" class="option">Sort by average rating</li>
                                                <li data-value="date" class="option">Sort by newness</li>
                                                <li data-value="price" class="option">Sort by price: low to high</li>
                                                <li data-value="price-desc" class="option">Sort by price: high to low</li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="shop-page-product-area tab-content">
                            <div id="grid" class="tab-pane fade in show active">
                                <div class="row">
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/1.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/2.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/3.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/13.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/5.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/6.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/7.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/8.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/9.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/10.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/11.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/12.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/6.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/7.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/8.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4 col-xl-3">
                                        <div class="single-product-area">
                                            <div class="product-wrapper gridview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <img src="{{asset('bege-v4/bege/images/product')}}/11.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Auctor gravida enim</a></h2> <span class="price">
                                                                        <del>$ 85.00</del> $ 75.00
                                                                    </span> </div>
                                                    <div class="product-hidden">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i></a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i></a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="list" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/1.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/2.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/3.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/4.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/5.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/6.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="{{asset('bege-v4/bege/images/product')}}/7.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="single-product-area">
                                            <div class="product-wrapper listview">
                                                <div class="list-col4">
                                                    <div class="product-image">
                                                        <a href=""> <span class="onsale">Sale!</span> <img src="images/product/8.jpg" alt=""> </a>
                                                        <div class="quickviewbtn"> <a href="#" data-toggle="modal" data-target="#product_modal" data-original-title="Quick View"><i class="fa fa-eye" aria-hidden="true"></i></a> </div>
                                                    </div>
                                                </div>
                                                <div class="list-col8">
                                                    <div class="product-info">
                                                        <h2><a href="single-product.html">Sit voluptatem</a></h2> <span class="price">
                                                                        <del>$ 77.00</del> $ 66.00
                                                                    </span>
                                                        <div class="product-rattings"> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star"></i></span> <span><i class="fa fa-star-half-o"></i></span> <span><i class="fa fa-star-o"></i></span> </div>
                                                        <div class="product-desc">
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                                        </div>
                                                    </div>
                                                    <div class="actions-wrapper">
                                                        <div class="add-to-cart"> <a href="cart.html">Add to cart</a> </div>
                                                        <div class="star-actions">
                                                            <ul class="actions">
                                                                <li><a href=""><i class="far fa-heart-o" aria-hidden="true"></i>Add to Wishlist</a></li>
                                                                <li><a href=""><i class="fa fa-random" aria-hidden="true"></i>Compare</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="woocommerce-pagination">
                        <ul class="page-numbers">
                            <li><span aria-current="page" class="page-numbers current">1</span></li>
                            <li><a class="page-numbers" href="#">2</a></li>
                            <li><a class="page-numbers" href="#">3</a></li>
                            <li><a class="page-numbers" href="#">4</a></li>
                            <li><a class="next page-numbers" href="#">→</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="quickview-wrapper">
    <!-- Modal -->
    <div class="modal fade show" id="product_modal" tabindex="-1" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-close-btn">
                    <button class="close" data-dismiss="modal">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <!-- Single product area -->
                    <div class="single-product-area">
                        <div class="container-fullwidth">
                            <div class="single-product-wrapper">
                                <div class="row">
                                    <div class="col-xs-12 col-md-7 col-lg-7">
                                        <div class="product-details-img-content">
                                            <div class="product-details-tab mr-40">
                                                <div class="product-details-large tab-content">
                                                    <div class="tab-pane active" id="pro-details1">
                                                        <div class="product-popup">
                                                            <a href="images/product/single/product4.jpg">
                                                                <img src="{{asset('bege-v4/bege/images/product/single')}}/product8.jpg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pro-details2">
                                                        <div class="product-popup">
                                                            <a href="images/product/single/product5.jpg">
                                                                <img src="{{asset('bege-v4/bege/images/product/single')}}/product7.jpg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pro-details3">
                                                        <div class="product-popup">
                                                            <a href="images/product/single/product6.jpg">
                                                                <img src="{{asset('bege-v4/bege/images/product/single')}}/product6 (1).jpg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pro-details4">
                                                        <div class="product-popup">
                                                            <a href="images/product/single/product7.jpg">
                                                                <img src="{{asset('bege-v4/bege/images/product/single')}}/product5 (1).jpg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="pro-details5">
                                                        <div class="product-popup">
                                                            <a href="images/product/single/product4 (1).jpg">
                                                                <img src="{{asset('bege-v4/bege/images/product/single')}}/product4 (1).jpg" alt="">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-details-small nav product-dec-slider-qui owl-carousel owl-loaded owl-drag">
                                                <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-477px, 0px, 0px); transition: all 0s ease 0s; width: 1909px;"><div class="owl-item cloned" style="width: 95.414px;"><a class="active" href="#pro-details1">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/1 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details2">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/2 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details3">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/3 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details4">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/4 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details5">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/5 (1).jpg" alt="">
                                                    </a></div><div class="owl-item active" style="width: 95.414px;"><a class="active" href="#pro-details1">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/6 (1).jpg" alt="">
                                                    </a></div><div class="owl-item active" style="width: 95.414px;"><a href="#pro-details2">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/7 (1).jpg" alt="">
                                                    </a></div><div class="owl-item active" style="width: 95.414px;"><a href="#pro-details3">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/8 (1).jpg" alt="">
                                                    </a></div><div class="owl-item active" style="width: 95.414px;"><a href="#pro-details4">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/9 (1).jpg" alt="">
                                                    </a></div><div class="owl-item" style="width: 95.414px;"><a href="#pro-details5">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/10 (1).jpg" alt="">
                                                    </a></div><div class="owl-item" style="width: 95.414px;"><a class="active" href="#pro-details1">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/11 (1).jpg" alt="">
                                                    </a></div><div class="owl-item" style="width: 95.414px;"><a href="#pro-details2">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/12 (1).jpg" alt="">
                                                    </a></div><div class="owl-item" style="width: 95.414px;"><a href="#pro-details3">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/13 (1).jpg" alt="">
                                                    </a></div><div class="owl-item" style="width: 95.414px;"><a href="#pro-details4">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/10 (1).jpg" alt="">
                                                    </a></div><div class="owl-item" style="width: 95.414px;"><a href="#pro-details5">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/9 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a class="active" href="#pro-details1">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/8 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details2">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/7 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details3">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/6 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details4">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/5 (1).jpg" alt="">
                                                    </a></div><div class="owl-item cloned" style="width: 95.414px;"><a href="#pro-details5">
                                                        <img src="{{asset('bege-v4/bege/images/product/thumbnails')}}/4 (1).jpg" alt="">
                                                    </a></div></div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><i class="ion-ios-arrow-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="ion-ios-arrow-right"></i></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-5 col-lg-5">
                                        <div class="single-product-info">
                                            <h1>Sit voluptatem</h1>
                                            <div class="product-rattings">
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star"></i></span>
                                                <span><i class="fa fa-star-half-o"></i></span>
                                                <span><i class="fa fa-star-o"></i></span>
                                            </div>
                                            <span class="price">
                                                <del>$ 77.00</del> $ 66.00
                                            </span>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus</p>
                                            <div class="box-quantity d-flex">
                                                <form action="#">
                                                    <input class="quantity mr-40" min="1" value="1" type="number">
                                                </form>
                                                <a class="add-cart" href="cart.html">add to cart</a>
                                            </div>
                                            <div class="wishlist-compear-area">
                                                <a href="wishlist.html"><i class="far fa-heart-o" aria-hidden="true"></i> Add to Wishlist</a>
                                                <a href="#"><i class="fa fa-refresh" aria-hidden="true"></i> Compare</a>
                                            </div>
                                            <div class="product_meta">
                                                <span class="posted_in">Categories: <a href="#" rel="tag">Accessories</a>, <a href="#" rel="tag">Clothings</a></span>
                                            </div>
                                            <div class="single-product-sharing">
                                                <div class="widget widget_socialsharing_widget">
                                                    <h3 class="widget-title">Share this product</h3>
                                                    <ul class="social-icons">
                                                        <li><a class="facebook social-icon" href="#"><i class="far fa-facebook"></i></a></li>
                                                        <li><a class="twitter social-icon" href="#"><i class="fa fa-twitter"></i></a></li>
                                                        <li><a class="pinterest social-icon" href="#"><i class="fa fa-pinterest"></i></a></li>
                                                        <li><a class="gplus social-icon" href="#"><i class="fa fa-google-plus"></i></a></li>
                                                        <li><a class="linkedin social-icon" href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Single product area end -->
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- END Modal -->
</div>
@endsection