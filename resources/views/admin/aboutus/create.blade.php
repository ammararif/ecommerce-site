@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif

                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-question">Add About Page Content</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('aboutus.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($about_data->id))
                                <input type="hidden" name="id" value="{{ @$about_data->id }}">
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter ..." value="{{ @$about_data->title }}">
                                    @error('title')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                                
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea class="form-control @error('description') is-invalid @enderror" name="description" rows="3" placeholder="Enter ...">{{ @$about_data->description }}</textarea>
                                    @error('description')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Image</label>
                                    <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" placeholder="Enter ..." value="{{ @$about_data->image }}">
                                    @error('image')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Our Happy Customer</label>
                                    <input type="number" name="happy_customer_counts" class="form-control @error('happy_customer_counts') is-invalid @enderror" placeholder="Enter ..." value="{{ @$about_data->happy_customer_counts }}">
                                    @error('happy_customer_counts')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Our Awards</label>
                                    <input type="number" name="awards_counts" class="form-control @error('awards_counts') is-invalid @enderror" placeholder="Enter ..." value="{{ @$about_data->awards_counts }}">
                                    @error('awards_counts')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Our Working Time</label>
                                    <input type="number" name="working_time" class="form-control @error('working_time') is-invalid @enderror" placeholder="Enter ..." value="{{ @$about_data->working_time }}">
                                    @error('working_time')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Our Products Reliability</label>
                                    <input type="number" name="reliable_products_counts" class="form-control @error('reliable_products_counts') is-invalid @enderror" placeholder="Enter ..." value="{{ @$about_data->reliable_products_counts }}">
                                    @error('reliable_products_counts')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('aboutus.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection