@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                
                <div class="col-md-12">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                    @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                        {{Session::get('error')}}
                    </div>
                    @endif

                    <div class="card mb-3">
                        <div class="card-header">
                        <i class="fas fa-table"></i>&nbsp;&nbsp;View About Page Content
                        <a href="{{url('admin/aboutus/create')}}" class="float-right btn btn-sm btn-dark">Add Data</a>
                        </div>
                        <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th style="min-width:30px;">#</th>
                                <th style="min-width:120px;" class="text-center">Title</th>
                                <th style="min-width:120px;" class="text-center">Description</th>
                                <th style="min-width:80px;" class="text-center">Image</th>
                                <th style="min-width:80px;" class="text-center">Our Happy Customer</th>
                                <th style="min-width:80px;" class="text-center">Our Awards</th>
                                <th style="min-width:80px;" class="text-center">Our Work Time</th>
                                <th style="min-width:80px;" class="text-center">Our Products Reliability</th>
                                <th style="min-width:170px;" colspan="2" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($about_data as $about)
                                <tr>
                                    <td>{{$about->id}}</td>
                                    <td>{{$about->title}}</td>
                                    <td>{{$about->description}}</td>
                                    <td>{{$about->happy_customer_counts}}</td>
                                    <td>{{$about->awards_counts}}</td>
                                    <td>{{$about->working_time}}</td>
                                    <td>{{$about->reliable_products_counts}}</td>
                                    <td align="center"><img src="{{ asset('/bege-v4/bege/images/about').'/'.$about->image }}" width="150px" /></td>
                                    <td align="center">
                                    <a class="btn btn-info btn-sm" href="{{route('aboutus.edit',$about->id)}}">Edit</a>
                                    <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger btn-sm" href="{{route('aboutus.delete',$about->id)}}">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection