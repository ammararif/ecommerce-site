@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif

                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-question">Add Portfolio Categories</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('portfolio_cat.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($portfolio_cat_data->id))
                                <input type="hidden" name="id" value="{{ @$portfolio_data->id }}">
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Portfolio Categories</label>
                                    <input type="text" name="portfolio_categories" class="form-control @error('portfolio_categories') is-invalid @enderror" placeholder="Enter ..." value="{{ @$portfolio_cat_data->portfolio_categories }}">
                                    @error('portfolio_categories')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('portfolio_cat.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection