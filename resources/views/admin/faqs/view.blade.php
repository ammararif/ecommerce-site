@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                    {{Session::get('success')}}
                </div>
                @elseif(Session::has('error'))
                <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                    {{Session::get('error')}}
                </div>
                @endif

                <div class=" col-md-12 mt-2">
                    <div class="card-header">
                        <h3 class="card-title">View All FAQs</h3>
                        <a href="{{route('faqs.create')}}" style="float: right" class="btn btn-danger">Add FAQ</a>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="col-12" id="accordion">
                            @foreach ($faqs as $item)
                                <div class="card card-danger card-outline">
                                    <a class="d-block w-100" data-toggle="collapse" href="#{{'collapse'.@$item->id}}">
                                        <div class="card-header">
                                            <h4 class="card-title w-100">
                                                {{@$item->question}}
                                            </h4>
                                        </div>
                                    </a>
                                    <div id="{{'collapse'.@$item->id}}" class="collapse" data-parent="#accordion">
                                        <div class="card-body">
                                            {{@$item->answer}}
                                        </div>
                                        <div class="m-2" style="float: right">

                                            <form action="{{route('faqs.delete',$item->id)}}" method="GET" class="delete-form">
                                                
                                                <a href="{{ route('faqs.edit',$item->id) }}" class="btn btn-primary">Edit</a>
                
                                                <input type="hidden" name="id" value="{{$item->id}}">
                                                <button type="submit" class="btn btn-danger delete-btn">Delete</button>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            @endforeach


                            
                        </div>

                        {{-- <div class="card">
                            <div class="card-header">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                Collapsible Group Item #1
                              </a>
                            </div>
                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                              <div class="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                              </div>
                            </div>
                          </div>

                          <div class="card">
                            <div class="card-header">
                              <a class="card-link" data-toggle="collapse" href="#collapseOne">
                                Collapsible Group Item #1
                              </a>
                            </div>
                            <div id="collapseOne" class="collapse show" data-parent="#accordion">
                              <div class="card-body">
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                              </div>
                            </div>
                          </div> --}}
                        {{-- <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>S No.</th>
                                    <th>Title</th>
                                    <th>Sub Title</th>
                                    <th>Description</th>
                                    <th>Logo</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $_i = 1
                                @endphp
                                @foreach ($data as $item)
                                <tr>
                                    <td>{{ $_i++ }}</td>
                        <td>{{ @$item->title }}</td>
                        <td>{{ @$item->sub_title }}</td>
                        <td>{{ @$item->description }}</td>
                        <td>
                            <img src="{{ @asset('website/images/blog')."/".$item->logo}}" height="60" width="auto"
                                alt="" srcset="">
                        </td>
                        <td>
                            <form action="{{route('blog.delete',$item->id)}}" method="POST" class="delete-form">
                                @csrf
                                <a href="{{ route('blog.edit',$item->id) }}" class="btn btn-warning mr-2"><i
                                        class="fas fa-edit"></i></a>

                                <input type="hidden" name="id" value="{{$item->id}}">
                                <button type="submit" class="btn btn-danger delete-btn"><i
                                        class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                        </tr>
                        @endforeach

                        </tbody>
                        </table> --}}
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection