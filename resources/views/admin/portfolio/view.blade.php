@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                
                <div class="col-md-12">
                    @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                    @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                        {{Session::get('error')}}
                    </div>
                    @endif

                    <div class="card mb-3">
                        <div class="card-header">
                        <i class="fas fa-table"></i>&nbsp;&nbsp;View All Portfolio
                        <a href="{{url('admin/portfolio/create')}}" class="float-right btn btn-sm btn-dark">Add Data</a>
                        </div>
                        <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                <th style="min-width:30px;">#</th>
                                <th style="min-width:120px;" class="text-center">Title</th>
                                <th style="min-width:80px;" class="text-center">Image</th>
                                <th style="min-width:170px;" colspan="2" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($portfolio_data as $portfolio)
                                <tr>
                                    <td>{{$portfolio->id}}</td>
                                    <td>{{$portfolio->title}}</td>
                                    <td align="center"><img src="{{ asset('/bege-v4/bege/images/portfolio').'/'.$portfolio->image }}" width="150px" /></td>
                                    <td align="center">
                                    <a class="btn btn-info btn-sm" href="{{route('portfolio.edit',$portfolio->id)}}">Edit</a>
                                    <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger btn-sm" href="{{route('portfolio.delete',$portfolio->id)}}">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection