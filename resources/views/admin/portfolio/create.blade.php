@extends('admin.layouts.app')
@section('content')

<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">

                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible col-md-12 mt-2">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fas fa-check"></i> Alert!</h5>
                        {{Session::get('success')}}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible col-md-12 mt-2">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fas fa-ban"></i> Alert!</h5>
                            {{Session::get('error')}}
                    </div>
                @endif

                <div class="col-md-12 mt-2">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-question">Add Portfolio</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form method="POST" action="{{route('portfolio.store')}}" enctype="multipart/form-data">
                            @csrf
                            @if (isset($portfolio_data->id))
                                <input type="hidden" name="id" value="{{ @$portfolio_data->id }}">
                            @endif
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Portfolio Categories</label>
                                    <select class="form-control @error('portfolio_categories') is-invalid @enderror" name="portfolio_categories" >
                                       
                                            @foreach(@$portfolio_cats as $portfolio_cat)
                                                @if(@$portfolio_cat->id == @$portfolio_data->portfolio_cat_id)
                                                    <option selected value="{{@$portfolio_cat->id}}">{{@$portfolio_cat->portfolio_categories}}</option>
                                                @else
                                                    <option value="{{@$portfolio_cat->id}}">{{@$portfolio_cat->portfolio_categories}}</option>
                                                @endif
                                            @endforeach

                                    </select>
                                    @error('portfolio_categories')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>
            
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" placeholder="Enter ..." value="{{ @$portfolio_data->title }}">
                                    @error('title')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <label for="">Image</label>
                                    <input type="file" name="image" class="form-control @error('image') is-invalid @enderror" placeholder="Enter ..." value="{{ @$portfolio_data->image }}">
                                    @error('image')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <a href="{{route('portfolio.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

@endsection