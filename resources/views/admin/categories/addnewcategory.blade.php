@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      
      <div>
        <h1>This is add category sections</h1>
      </div>
      
      <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i> Add Category
              <a href="{{url('admin/category')}}" class="float-right btn btn-sm btn-dark">All Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">

              @if(Session::has('category_added'))
              <span>{{Session::get('category_added')}}</span>
              @endif
                <form method="post" action="{{route('admin.storecategory')}}" enctype="multipart/form-data">
                  @csrf
                  <table class="table table-bordered">
                      <tr>
                          <th>Category Name</th>
                          <td><input type="text" name="name" class="form-control" /></td>
                      </tr>
                      <tr>
                          <th>Category Slug</th>
                          <td><input type="text" name="slug" class="form-control" /></td>
                      </tr>
                      <tr>
                          <td colspan="2">
                              <input type="submit" class="btn btn-primary" />
                          </td>
                      </tr>
                  </table>
                </form>
              </div>
            </div>
          </div>

        
    </section>
    <!-- /.content -->
  </div>
@endsection