@extends('admin.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      
      <div>
        <h1>This is category section</h1>
      </div>
      <div class="card mb-3">
    <div class="card-header">
      <i class="fas fa-table"></i> Categories
      <a href="{{url('admin/category/create')}}" class="float-right btn btn-sm btn-dark">Add Data</a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>#</th>
              <th class="text-center">Title</th>
              <th class="text-center">Image</th>
              <th class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>
              
              <tr>
                <td></td>
                <td></td>
                <td align="center"><img src="" width="150px" /></td>
                <td align="center">
                  <a class="btn btn-info btn-sm" href="">Update</a>
                  <a onclick="return confirm('Are you sure you want to delete?')" class="btn btn-danger btn-sm" href="">Delete</a>
                </td>
              </tr>
              
          </tbody>
        </table>
      </div>
    </div>
  </div>
    </section>
    <!-- /.content -->
  </div>
@endsection