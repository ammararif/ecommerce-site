<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"website\HomeController@index")->name('home');
Route::get('/shop',"website\HomeController@shop");
Route::get('/cart',"website\HomeController@cart");
Route::get('/checkout',"website\HomeController@checkout");
Route::get('/faqs',"website\HomeController@faqs");
Route::get('/portfolio',"website\HomeController@portfolio");
Route::get('/about',"website\HomeController@about");
Route::get('/contact',"website\HomeController@contact");
Route::get('/blog',"website\HomeController@blog");
Route::get('/wishlist',"website\HomeController@wishlist");
Route::get('/404',"website\HomeController@error");

Route::get('/login',"website\AuthController@loginShow")->name('login')->middleware('guest:web');
Route::post('/login',"website\AuthController@loginPost")->name('login.store');
Route::get('/logout',"website\AuthController@logout")->name('logout')->middleware('auth:web');

Route::middleware('auth:web')->group(function () {
    Route::get('home',"website\HomeController@index")->name('home');
});


Route::get('/admin/login',"Admin\AuthController@loginShow")->name('admin.login')->middleware('guest:admin');
Route::post('/admin/login',"Admin\AuthController@loginPost")->name('admin.login.store');
Route::get('/admin/logout',"Admin\AuthController@logout")->name('admin.logout')->middleware('auth:admin');


Route::get('/forgotpassword/{type}',            "ForgetPasswordResetController@viewForgotpassword")                 ->name('forgotpassword');
Route::get('/resetPassword/{email_encode}',     "ForgetPasswordResetController@ResetPassword")                      ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  "ForgetPasswordResetController@SetNewPassword")                     ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  "ForgetPasswordResetController@AjaxCallForForgotPasswordEmail")     ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

Route::get('register',"website\RegisterController@viewRegister")->name("register");
Route::post('register',"website\RegisterController@registerUser")->name("registerUser");



Route::middleware('auth:admin')->group(function () {
    Route::get('admin/home',"Admin\HomeController@index")->name('admin.home');
    Route::get('admin/categories/categories',"Admin\CategoryController@index")->name('admin.categories');
    Route::get('admin/categories/addnewcategory',"Admin\CategoryController@addCategory")->name('admin.addnewcategory');
    Route::post('admin/categories/addnewcategory',"Admin\CategoryController@storeCategory")->name('admin.storecategory');

    // faqs
    Route::get( 'admin/faqs/view',       'Admin\FaqController@index')       ->name('faqs.view');
    Route::get( 'admin/faqs/create',     'Admin\FaqController@create')      ->name('faqs.create');
    Route::get( 'admin/faqs/edit/{id}',   'Admin\FaqController@edit')        ->name('faqs.edit');
    Route::post('admin/faqs/store',       'Admin\FaqController@store')       ->name('faqs.store');
    Route::get( 'admin/faqs/delete/{id}', 'Admin\FaqController@delete')      ->name('faqs.delete');
    // About Us
    Route::get( 'admin/aboutus/view',        'Admin\AboutController@index')       ->name('aboutus.view');
    Route::get( 'admin/aboutus/create',      'Admin\AboutController@create')      ->name('aboutus.create');
    Route::get( 'admin/aboutus/edit/{id}',   'Admin\AboutController@edit')        ->name('aboutus.edit');
    Route::post('admin/aboutus/store',       'Admin\AboutController@store')       ->name('aboutus.store');
    Route::get( 'admin/aboutus/delete/{id}', 'Admin\AboutController@delete')      ->name('aboutus.delete');
    // Portfolio Category
    Route::get( 'admin/portfolio_category/view',        'Admin\PortfolioCategoryController@index')       ->name('portfolio_cat.view');
    Route::get( 'admin/portfolio_category/create',      'Admin\PortfolioCategoryController@create')      ->name('portfolio_cat.create');
    Route::get( 'admin/portfolio_category/edit/{id}',   'Admin\PortfolioCategoryController@edit')        ->name('portfolio_cat.edit');
    Route::post('admin/portfolio_category/store',       'Admin\PortfolioCategoryController@store')       ->name('portfolio_cat.store');
    Route::get( 'admin/portfolio_category/delete/{id}', 'Admin\PortfolioCategoryController@delete')      ->name('portfolio_cat.delete');
    // Portfolio
    Route::get( 'admin/portfolio/view',        'Admin\PortfolioController@index')       ->name('portfolio.view');
    Route::get( 'admin/portfolio/create',      'Admin\PortfolioController@create')      ->name('portfolio.create');
    Route::get( 'admin/portfolio/edit/{id}',   'Admin\PortfolioController@edit')        ->name('portfolio.edit');
    Route::post('admin/portfolio/store',       'Admin\PortfolioController@store')       ->name('portfolio.store');
    Route::get( 'admin/portfolio/delete/{id}', 'Admin\PortfolioController@delete')      ->name('portfolio.delete');
});

